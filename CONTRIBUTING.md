# Contributing

Contributions are welcome! Please first discuss the change you wish to make via issue or email before making a change.

## GitHub Mirror

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/datadyne/colourize/issues).

## Roadmap

- [ ] Implement all CLI items
- [ ] Add support for general datasets
- [ ] Allow train/val/test splits
- [ ] Support output to non-pytorch format
