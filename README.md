## Colourize

> Convert a dataset of images into many different colourspaces.

A command line tool to preprocess a dataset into many different colourspaces.
Also provides a PyTorch `Dataset` that will work on the output and a PyTorch
transform to do the conversion online during training instead.

### Currently in alpha this is just a MWE
